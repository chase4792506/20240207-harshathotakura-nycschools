package com.example.a20240207_harshathotakura_nycschools.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a20240207_harshathotakura_nycschools.ui.repository.SchoolRepoRepositoryInterface

class SchoolRepoViewModelFactory(
    private val repository: SchoolRepoRepositoryInterface
) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SchoolRepoViewModel(repository) as T
    }
}