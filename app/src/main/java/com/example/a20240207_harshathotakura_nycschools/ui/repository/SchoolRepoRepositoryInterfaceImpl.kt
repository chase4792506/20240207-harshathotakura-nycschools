package com.example.a20240207_harshathotakura_nycschools.ui.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.a20240207_harshathotakura_nycschools.data.interactor.LocalDataSource
import com.example.a20240207_harshathotakura_nycschools.data.interactor.NetworkDataSource
import com.example.a20240207_harshathotakura_nycschools.data.model.NetworkState
import com.example.a20240207_harshathotakura_nycschools.data.model.SATResponse
import com.example.a20240207_harshathotakura_nycschools.data.model.SchoolResponse


class SchoolRepoRepositoryInterfaceImpl(
    private val localDS: LocalDataSource,
    private val networkDS: NetworkDataSource
) : SchoolRepoRepositoryInterface {


    private val repoMediatorLiveData = MediatorLiveData<List<SchoolResponse>>()
    override val schoolRepo: LiveData<List<SchoolResponse>> get() = repoMediatorLiveData

    private val networkMediatorLiveData = MediatorLiveData<NetworkState>()
    override val networkState: LiveData<NetworkState> get() = networkMediatorLiveData

    override val schoolSATRepo: LiveData<List<SATResponse>>
        get() = networkDS.schoolSATRepo


    private val apiNetworkState = networkDS.networkState
    private val dbNetworkState = localDS.networkState
    private val networkRepo = networkDS.schoolRepo
    private val localRepo = localDS.schoolRepo

    init {
        networkMediatorLiveData.addSource(apiNetworkState) {
            networkMediatorLiveData.postValue(it)
        }
        networkMediatorLiveData.addSource(dbNetworkState) {
            networkMediatorLiveData.postValue(it)
        }
        repoMediatorLiveData.addSource(networkRepo) {
            repoMediatorLiveData.postValue(it)
        }
        repoMediatorLiveData.addSource(localRepo) {
            repoMediatorLiveData.postValue(it)
        }
    }

    override fun getSchoolRepoList() {
        if (isNetworkCallNeeded()) {
            fetchDataFromNetwork()
        } else {
            Log.e("SchoolRepoImpl", "getting from local database")
            localDS.getLocalData()
        }
    }

    override fun getSATSchool(id: String) {
        networkDS.fetchSATScores(id)
    }

    override fun fetchDataFromNetwork() {
        Log.e("SchoolRepoImpl", "getting from network")
        networkDS.fetchSchoolRepo()

    }

    // Refresh data if it's 20 min old
    private fun isNetworkCallNeeded(): Boolean {
        val current = System.currentTimeMillis()
        val storedTime = localDS.getFirstItem()
        return if (storedTime == null) return true else (current.minus(storedTime.updateTime) > 7.2e+6)
    }



}