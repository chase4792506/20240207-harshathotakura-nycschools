package com.example.a20240207_harshathotakura_nycschools.ui.repository

import androidx.lifecycle.LiveData
import com.example.a20240207_harshathotakura_nycschools.data.model.NetworkState
import com.example.a20240207_harshathotakura_nycschools.data.model.SATResponse
import com.example.a20240207_harshathotakura_nycschools.data.model.SchoolResponse


interface SchoolRepoRepositoryInterface {
    fun getSchoolRepoList ()
    fun getSATSchool(id:String)
    fun fetchDataFromNetwork()// using this method to do refresh - get refresh data
    val schoolRepo: LiveData<List<SchoolResponse>>
    val networkState: LiveData<NetworkState>
    val schoolSATRepo:LiveData<List<SATResponse>>
}