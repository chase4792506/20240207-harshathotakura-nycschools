package com.example.a20240207_harshathotakura_nycschools.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.Group
import androidx.recyclerview.widget.RecyclerView
import com.example.a20240207_harshathotakura_nycschools.R
import com.example.a20240207_harshathotakura_nycschools.TrendingRepoActivity
import com.example.a20240207_harshathotakura_nycschools.data.model.SchoolResponse


class RecyclerAdapter(
    private val list: ArrayList<SchoolResponse>
) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    private var mExpandedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recycler_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])

        val group: Group = holder.itemView.findViewById(R.id.group)
        /*group.setVisibility(if (isExpanded) View.VISIBLE else View.GONE)
        holder.itemView.isActivated = isExpanded*/
        // val isExpanded = position === mExpandedPosition

        holder.itemView.setOnClickListener {
            /* mExpandedPosition = if (isExpanded) -1 else position
             notifyDataSetChanged()*/

            if (group.context is TrendingRepoActivity) {
                val activity: TrendingRepoActivity = group.context as TrendingRepoActivity
                getList()[position].dbn?.let { it1 ->
                    getList()[position].school_name?.let { it2 ->
                        activity.getSATList(
                            it1,
                            it2
                        )
                    }
                }
            }

        }
    }

    fun resetData(data: List<SchoolResponse>) {
        list.clear()
        list.addAll(data)
        mExpandedPosition = -1
        notifyDataSetChanged()
    }

    fun getList(): List<SchoolResponse> {
        return list
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            data: SchoolResponse
        ) {
            val txtName: TextView = itemView.findViewById(R.id.txt_name)
            val address: TextView = itemView.findViewById(R.id.txt_heading)
            val city: TextView = itemView.findViewById(R.id.txt_desc)
            val textZip: TextView = itemView.findViewById(R.id.txt_zip)
            val state:TextView = itemView.findViewById(R.id.txt_state)

            txtName.text = data.school_name
            address.text = data.primary_address_line_1
            city.text = data.city
            textZip.text = data.zip
            state.text = data.state_code


            /*
            TODO -- For the future in case of images are in the RESET Call to download
             */
            /*  Glide.with(itemView.context).load(data.avatar)
                  .apply(RequestOptions.circleCropTransform())
                  .into(imageView)*/
        }

    }
}