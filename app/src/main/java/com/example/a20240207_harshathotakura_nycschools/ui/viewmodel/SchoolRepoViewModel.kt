package com.example.a20240207_harshathotakura_nycschools.ui.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import com.example.a20240207_harshathotakura_nycschools.data.model.NetworkState
import com.example.a20240207_harshathotakura_nycschools.data.model.SATResponse
import com.example.a20240207_harshathotakura_nycschools.data.model.SchoolResponse
import com.example.a20240207_harshathotakura_nycschools.ui.repository.SchoolRepoRepositoryInterface

class SchoolRepoViewModel(
    private val repository: SchoolRepoRepositoryInterface
) : ViewModel() {

    private val reposMediatorLiveData = MediatorLiveData<List<SchoolResponse>>()
    val schoolRepo: LiveData<List<SchoolResponse>> get() = reposMediatorLiveData
    val schoolSATRepo: LiveData<List<SATResponse>> get() = repository.schoolSATRepo

    init {
        reposMediatorLiveData.addSource(repository.schoolRepo){
            reposMediatorLiveData.value = it
        }
    }

    fun getSchoolRepo() {
         repository.getSchoolRepoList()
    }

    val networkState: LiveData<NetworkState> by lazy {
        repository.networkState
    }

    fun fetchDataFromNetwork() {
        repository.fetchDataFromNetwork()
    }

    fun getSATScores(id:String){
        repository.getSATSchool(id)
    }


    @SuppressLint("CheckResult")
    fun filterList(
        isSortByCity: Boolean,
        list: List<SchoolResponse>
    ) {
        Observable.just(list)
            .subscribeOn(Schedulers.io())
            .subscribe {
                if (isSortByCity) {
                    val sortedList = it.sortedWith(compareBy { it.city })
                    reposMediatorLiveData.postValue(sortedList)
                } else {
                    val sortedList = it.sortedWith(compareBy { it.school_name })
                    reposMediatorLiveData.postValue(sortedList)
                }
            }
    }

}