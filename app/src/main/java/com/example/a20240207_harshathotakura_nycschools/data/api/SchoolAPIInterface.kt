package com.example.a20240207_harshathotakura_nycschools.data.api


import com.example.a20240207_harshathotakura_nycschools.data.model.SATResponse
import com.example.a20240207_harshathotakura_nycschools.data.model.SchoolResponse
import com.google.gson.JsonElement
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*


interface SchoolAPIInterface {

    @GET("s3k6-pzi2.json")
    fun schoolRepoListGETCall(): Observable<List<SchoolResponse>>

    @GET("f9bf-2cp4.json")
    fun schoolRepoSATGETCall(@Query(value = "dbn",encoded = true)id:String?): Observable<List<SATResponse>>

}