package com.example.a20240207_harshathotakura_nycschools.data.interactor

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.a20240207_harshathotakura_nycschools.data.api.SchoolAPIInterface
import com.example.a20240207_harshathotakura_nycschools.data.model.NetworkState
import com.example.a20240207_harshathotakura_nycschools.data.model.SATResponse
import com.example.a20240207_harshathotakura_nycschools.data.model.SchoolResponse
import io.reactivex.schedulers.Schedulers


class NetworkDataSource(
    private val apiService: SchoolAPIInterface,
    private val localDS: LocalDataSource
) {

    private val _networkState = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState> get() = _networkState

    private val _schoolRepo = MutableLiveData<List<SchoolResponse>>()
    val schoolRepo: LiveData<List<SchoolResponse>> get() = _schoolRepo

    private val _schoolSATRepo = MutableLiveData<List<SATResponse>>()
    val schoolSATRepo: LiveData<List<SATResponse>> get() = _schoolSATRepo

    @SuppressLint("CheckResult")
    fun fetchSchoolRepo() {
        try {
            _networkState.postValue(NetworkState.LOADING)
            apiService.schoolRepoListGETCall()
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        _networkState.postValue(NetworkState.LOADED)
                        _schoolRepo.postValue(it)
                        localDS.saveData(it)
                    },
                    {
                        _networkState.postValue(NetworkState.ERROR)
                        Log.e("NetworkDataSource", it.message.toString())
                    }
                )

        } catch (e: Exception) {
            _networkState.postValue(NetworkState.ERROR)
            Log.e("NetworkDataSource", e.message.toString())
        }
    }

    @SuppressLint("CheckResult")
    fun fetchSATScores(id:String) {
        try {
            _networkState.postValue(NetworkState.LOADING)
            apiService.schoolRepoSATGETCall(id)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        _networkState.postValue(NetworkState.LOADED)
                        _schoolSATRepo.postValue(it)
                        //localDS.saveData(it)
                    },
                    {
                        _networkState.postValue(NetworkState.ERROR)
                        Log.e("NetworkDataSource", it.message.toString())
                    }
                )

        } catch (e: Exception) {
            _networkState.postValue(NetworkState.ERROR)
            Log.e("NetworkDataSource", e.message.toString())
        }
    }

}