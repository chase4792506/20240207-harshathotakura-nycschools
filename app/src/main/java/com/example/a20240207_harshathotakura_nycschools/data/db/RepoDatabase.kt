package com.example.a20240207_harshathotakura_nycschools.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.a20240207_harshathotakura_nycschools.data.model.SchoolResponse

@Database(entities = arrayOf(SchoolResponse::class), version = 2)
abstract class RepoDatabase: RoomDatabase() {

    abstract fun repoDao(): RepoDao

    companion object {
        @Volatile private var instance: RepoDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                RepoDatabase::class.java, "githubRepoDatabase.db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
    }
}