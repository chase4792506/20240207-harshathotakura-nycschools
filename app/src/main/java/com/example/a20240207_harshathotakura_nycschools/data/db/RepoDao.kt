package com.example.a20240207_harshathotakura_nycschools.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.a20240207_harshathotakura_nycschools.data.model.SchoolResponse

@Dao
interface RepoDao {
    @Query("SELECT * FROM schoolresponse")
    fun getAll(): List<SchoolResponse>

    @Query("SELECT * FROM schoolresponse WHERE id LIKE :id")
    fun findById(id: Int): SchoolResponse

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(data: List<SchoolResponse>)

    @Query("DELETE FROM schoolresponse")
    fun deleteAll()
}