package com.example.a20240207_harshathotakura_nycschools.data.model

enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}

/**
 * Network States
 */
class NetworkState(val status: Status, val msg: String) {
    companion object {
        val LOADED: NetworkState = NetworkState(Status.SUCCESS, "Success")
        val LOADING: NetworkState = NetworkState(Status.RUNNING, "Running")
        val ERROR: NetworkState = NetworkState(Status.FAILED, "Something went wrong")

    }
}