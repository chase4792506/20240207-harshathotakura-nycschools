package com.example.a20240207_harshathotakura_nycschools.data.model


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class SchoolResponse(
    @PrimaryKey var id: Int,

    @ColumnInfo(name = "update_time")
    var updateTime: Long,

    @ColumnInfo(name = "primary_address_line_1")
    val primary_address_line_1: String?,
    @ColumnInfo(name = "city")
    val city: String?,
    @ColumnInfo(name = "zip")
    val zip: String?,
    @ColumnInfo(name = "state_code")
    val state_code: String?,
    @ColumnInfo(name = "school_name")
    val school_name: String?,
    @ColumnInfo(name = "council_district")
    val council_district: Int?,
    @ColumnInfo(name = "website")
    val website: String?,
    @ColumnInfo(name = "bdn")
    val dbn:String?

)