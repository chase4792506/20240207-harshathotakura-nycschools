package com.example.a20240207_harshathotakura_nycschools.data.interactor

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.a20240207_harshathotakura_nycschools.data.db.RepoDao
import com.example.a20240207_harshathotakura_nycschools.data.model.NetworkState
import com.example.a20240207_harshathotakura_nycschools.data.model.SchoolResponse
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class LocalDataSource(
    private val repoDao: RepoDao
) {

    private val _schoolRepos = MutableLiveData<List<SchoolResponse>>()
   val schoolRepo: LiveData<List<SchoolResponse>> get() = _schoolRepos

    private val _networkState = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState> get() = _networkState

    fun saveData(list: List<SchoolResponse>?) {
        list?.mapIndexed { index, response ->
            response.id = index
            response.updateTime = System.currentTimeMillis()
        }
        repoDao.deleteAll()
        repoDao.insertAll(list!!)
        Log.e("LocalDataSource", "data inserted Success")
    }

    @SuppressLint("CheckResult")
    fun getLocalData() {
        Observable.just(repoDao)
            .subscribeOn(Schedulers.io())
            .delay(1, TimeUnit.SECONDS)
            .subscribe {
                _networkState.postValue(NetworkState.LOADING)
                val list = it.getAll()
                _networkState.postValue(NetworkState.LOADED)
                _schoolRepos.postValue(list)
            }
    }

    fun getFirstItem(): SchoolResponse {
        return repoDao.findById(0)
    }
}