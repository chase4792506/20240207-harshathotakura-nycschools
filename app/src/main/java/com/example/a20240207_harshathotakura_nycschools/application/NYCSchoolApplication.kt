package com.example.a20240207_harshathotakura_nycschools.application

import android.app.Application
import com.example.a20240207_harshathotakura_nycschools.data.api.SchoolAPIInterface
import com.example.a20240207_harshathotakura_nycschools.data.api.SchoolRepoRestClient
import com.example.a20240207_harshathotakura_nycschools.data.db.RepoDatabase
import com.example.a20240207_harshathotakura_nycschools.data.interactor.LocalDataSource
import com.example.a20240207_harshathotakura_nycschools.data.interactor.NetworkDataSource
import com.example.a20240207_harshathotakura_nycschools.ui.RecyclerAdapter
import com.example.a20240207_harshathotakura_nycschools.ui.repository.SchoolRepoRepositoryInterface
import com.example.a20240207_harshathotakura_nycschools.ui.repository.SchoolRepoRepositoryInterfaceImpl
import com.example.a20240207_harshathotakura_nycschools.ui.viewmodel.SchoolRepoViewModelFactory

import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class NYCSchoolApplication : Application(), KodeinAware {

    override val kodein: Kodein = Kodein.lazy {
        import(androidXModule(this@NYCSchoolApplication))

        bind<SchoolAPIInterface>() with singleton { SchoolRepoRestClient.getClient() }

        bind() from singleton { instance<RepoDatabase>().repoDao() }
        bind() from  singleton { RepoDatabase(instance()) }

        bind() from singleton { LocalDataSource(instance()) }

        bind() from  singleton { NetworkDataSource(instance(), instance()) }
        bind<SchoolRepoRepositoryInterface>() with singleton { SchoolRepoRepositoryInterfaceImpl(instance(), instance()) }
        bind() from provider { SchoolRepoViewModelFactory(instance()) }

        bind() from singleton { RecyclerAdapter(ArrayList()) }
    }
}