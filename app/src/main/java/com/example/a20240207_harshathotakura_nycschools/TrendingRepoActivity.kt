package com.example.a20240207_harshathotakura_nycschools

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.a20240207_harshathotakura_nycschools.ui.viewmodel.SchoolRepoViewModel
import com.example.a20240207_harshathotakura_nycschools.data.model.NetworkState
import com.example.a20240207_harshathotakura_nycschools.data.model.Status
import com.example.a20240207_harshathotakura_nycschools.ui.RecyclerAdapter
import com.example.a20240207_harshathotakura_nycschools.ui.viewmodel.SchoolRepoViewModelFactory
import com.facebook.shimmer.ShimmerFrameLayout
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance


class TrendingRepoActivity : AppCompatActivity(), KodeinAware {
    override val kodein: Kodein by closestKodein()

    private lateinit var shimmerFrameLayout: ShimmerFrameLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var btn: Button
    private lateinit var swipeContainerLayout:SwipeRefreshLayout
    private lateinit var constrainLayout:ConstraintLayout
    private lateinit var infoConstrainLayout:ConstraintLayout
    private lateinit var schoolNameValue:TextView
    private lateinit var mathSATValue:TextView
    private lateinit var readingSATValue:TextView
    private lateinit var writingSATValue:TextView
    private lateinit var okBtn:Button

    private val factory: SchoolRepoViewModelFactory by instance()
     val viewModel: SchoolRepoViewModel by lazy {
        ViewModelProvider(this, factory).get(SchoolRepoViewModel::class.java)
    }
    private val adapter: RecyclerAdapter by instance()
    private var networkState: NetworkState = NetworkState.LOADED


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trending_repo)


        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setUpViews()
        setSupportActionBar(toolbar)

        shimmerFrameLayout.startShimmerAnimation()
        setUpRecyclerView()
        getData()
        handlePullToRefresh()
        handleRetryAndOk()
    }

    /*
    ToDo Really should be using View Binding - Jetpack Library
     */
    private fun setUpViews() {
        shimmerFrameLayout = findViewById(R.id.parent_shimmer_layout)
        recyclerView = findViewById(R.id.recyclerview)
        btn = findViewById(R.id.button_retry)
        swipeContainerLayout = findViewById(R.id.swipe_container)
        constrainLayout =findViewById(R.id.layout_error)
        infoConstrainLayout = findViewById(R.id.info_layout)
        schoolNameValue = findViewById(R.id.school_name)
        readingSATValue = findViewById(R.id.tv_reading)
        mathSATValue = findViewById(R.id.tv_math)
        writingSATValue = findViewById(R.id.tv_writing)
        okBtn = findViewById(R.id.ok_btn)
    }

    private fun setUpRecyclerView() {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    private fun getData() {
        viewModel.schoolRepo.observe(this, Observer {
            if (it != null) adapter.resetData(it)
        })

        viewModel.networkState.observe(this, Observer {
            networkState = it
            if (it.status == Status.FAILED){
                handleLayout(View.GONE, View.GONE, View.VISIBLE)
            }else if (it.status == Status.SUCCESS){
                handleLayout(View.VISIBLE, View.GONE, View.GONE)
            }
            Log.e("TrendingRepoActivity", "" + it.msg)
        })

        viewModel.getSchoolRepo()
    }

    fun getSATList(id: String, schoolName: String){
        viewModel.schoolSATRepo.observe(this) {
            if (!it.isNullOrEmpty()) {
                schoolNameValue.text = it[0].schoolName
                mathSATValue.text = getString(R.string.avg_math_score, it[0].satMathAvgScore)
                readingSATValue.text = getString(R.string.avg_reading_score, it[0].satCriticalReadingAvgScore)
                writingSATValue.text = getString(R.string.avg_writing_score, it[0].satWritingAvgScore)
                recyclerView.visibility = View.GONE
                infoConstrainLayout.visibility = View.VISIBLE
                handleLayout(View.GONE, View.GONE, View.GONE)
            }else{

                schoolNameValue.text = schoolName
                mathSATValue.text = getString(R.string.avg_math_score, "N/A")
                readingSATValue.text = getString(R.string.avg_reading_score, "N/A")
                writingSATValue.text = getString(R.string.avg_writing_score, "N/A")
                recyclerView.visibility = View.GONE
                infoConstrainLayout.visibility = View.VISIBLE
                handleLayout(View.GONE, View.GONE, View.GONE)
            }
        }

        viewModel.getSATScores(id)
    }

    private fun handleRetryAndOk() {
        btn.setOnClickListener {
            handleLayout(View.GONE, View.VISIBLE, View.GONE)
            shimmerFrameLayout.startShimmerAnimation()
            fetchFreshData()
        }
        okBtn.setOnClickListener{
            handleLayout(View.VISIBLE, View.GONE, View.GONE)
            recyclerView.visibility = View.VISIBLE
            infoConstrainLayout.visibility = View.GONE
        }

    }

    private fun handleLayout(swipeContainer: Int, shimmerLayout: Int, error: Int) {
        shimmerFrameLayout.stopShimmerAnimation()
        swipeContainerLayout.isRefreshing = false
        swipeContainerLayout.visibility = swipeContainer
        shimmerFrameLayout.visibility = shimmerLayout
        constrainLayout.visibility = error
    }

    private fun handlePullToRefresh() {
        swipeContainerLayout.setOnRefreshListener(this::fetchFreshData)
    }

    private fun fetchFreshData() {
        if (networkState.status !=  Status.RUNNING) viewModel.fetchDataFromNetwork()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuSortByStars -> {
                viewModel.filterList(true, adapter.getList())
            }
            R.id.menuSortByNames -> {
                viewModel.filterList(false, adapter.getList())
            }
        }
        return true
    }

    override fun onStop() {
        super.onStop()
        shimmerFrameLayout.stopShimmerAnimation()
    }

}
